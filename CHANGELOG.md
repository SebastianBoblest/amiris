<!-- SPDX-FileCopyrightText: 2022 German Aerospace Center <amiris@dlr.de>

SPDX-License-Identifier: CC0-1.0 -->

# [Agua - v1.3 (tba)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.3):
Added: IndividualPlantBuilder - allows to parameterise individual conventional power plants from a list
Updated: StorageTrader with Strategist "DISPATCH_FILE" can now also provide forecasts
Fixes: improved packaging

## Minor changes
Added: REUSE license statements for each file in repository
Added: automatic unit testing infrastructure

# [Adams - v1.2 (2022-02-14)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.2):
Harmonised inputs & outputs across agents

# [Acatenango - v1.1 (2022-01-21)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.1): 
Harmonised contracting for Forecaster and Exchange

# [Acamarachi - v1.0 (2021-12-06)](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/releases/v1.0): 
Initial release of AMIRIS

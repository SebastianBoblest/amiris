<!-- SPDX-FileCopyrightText: 2022 German Aerospace Center <amiris@dlr.de>

SPDX-License-Identifier: Apache-2.0 -->
# Welcome
Welcome fellow researcher.
We are happy that you want to contribute to and improve AMIRIS.

## General information
AMIRIS is a software that is open for your contribution.
It is an interdisciplinary, open source project, thus help is always appreciated!
The development happens openly on GitLab and is supplemented by online developer meetings to discuss more complicated topics.

### Release plan
A detailed planning process regarding releases is not yet defined.
Minor bug fixes and new features will be continuously released.

## What is where?
Here you can find information on how to contribute.
For an overview of AMIRIS in general please visit the [AMIRIS-Wiki](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/wikis/home).
There, you can also find relevant background information like a glossary and a description of existing model components.
AMIRIS is based on FAME - an open Framework for distributed Agent-based Modelling of Energy systems.
Please visit [FAME on gitlab](https://gitlab.com/fame-framework) to get more details on that framework.

Installation instructions can be found in the [ReadMe](README.md) file.

Issues and Bugs are tracked on the [Repository](https://gitlab.com/dlr-ve/esy/amiris/amiris/-/issues) at GitLab.

We kindly ask you to discuss the AMIRIS development and configuration at the [openMod Forum](https://forum.openmod.org/tag/amiris).

## Contact / Help
You can contact the main developers via [amiris@dlr.de](mailto:amiris@dlr.de) to pose any questions or to get involved.

# How to Contribute
You are welcome to contribute to AMIRIS via bug reports and feature requests at any time.
Please note that code contributions (e.g. via pull requests) require you to fill in the [Contributor License Agreement](CLA.md), before we can merge your code into the project.

## Feature request
1. Please check the open issue list to see whether an issue similar to your feature idea already exists.
2. If no similar issue exists: Open a new issue using the *feature_request* template and fill in the corresponding items.
3. You may contact the main developers via [amiris@dlr.de](mailto:amiris@dlr.de) to discuss the issue with us.

## Bug report
1. Please check the open issue list to see whether a similar bug has already been reported.
2. If no similar bug was reported yet: Open a new issue using the *bug_report* template and fill in the corresponding items.
3. You may contact the main developers via [amiris@dlr.de](mailto:amiris@dlr.de) to discuss the issue with us.

## Code contributions
Thank you for your intention to contribute.

### Environment
It is recommended to use a current Eclipse version to compile AMIRIS code.
[Maven](https://maven.apache.org/) is strongly recommended for resolving the dependencies of AMIRIS.
We recommend Java 8 (64 bit) to build and run AMIRIS.

### Coding
#### Coding Conventions
Please follow the [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).
The file "CodeStyle.xml" in the "misc" folder of the repository covers most of Google's specifications.

### Before submitting
Please, check the following points before submitting a pull request:
1. Please fill in the [Contributor License Agreement](CLA.md) and send it to [amiris@dlr.de](mailto:amiris@dlr.de).
1. Ensure there is a corresponding issue to your code contribution.
1. Make sure your code is based on the latest version of the *main* branch and that there are no conflicts. In case of conflicts: fix them first.
1. Make sure that existing unit tests are all successful. Add new unit tests that cover your code contributions and ensure that your code works as intended. In case an error occurred that you don't know how to solve, ask for help from the main developers.

### Pull request
1. Submit your request using the provided *pull_request* template & (briefly) describe the changes you made in the pull request.
1. Contact the main developers via [amiris@dlr.de](mailto:amiris@dlr.de) or at [openMod Forum](https://forum.openmod.org/tag/amiris) in case you've got any questions.

# List of Contributors
see file [Citation.cff](CITATION.cff)
